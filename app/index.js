var child_process = require('child_process');
const fs = require('fs');

const dataFolder = './app/data/';

var target_list = document.getElementById("target_list");
var bar = document.getElementById('js_progressbar');

const filename_regex = /^(?<cap>cap)\.(?<person>[^\.]+)\.(?<caption>[^\.]+)\.(?<finger>f[^\.]+)\.(?<shift>s[0-9])\.(?<gain>g[0-9])\.png/;

function make_item_text_desc(el, field_name, field_value){
    //<span class="uk-label">Default</span>
    var text_desc = document.createElement('span');
    text_desc.className = "uk-label";
    text_desc.textContent = field_value;
    text_desc.style.marginRight = "60px";
    text_desc.setAttribute("uk-tooltip", field_name );
    el.appendChild(text_desc);
}

function make_item_html(obj, filename){
    var item = document.createElement('div');
    item.className = "uk-container";

    //<span class="uk-label">Default</span>
    var hh = document.createElement('div');
    hh.textContent = filename;
    item.appendChild(hh);

    //div
    var flex = document.createElement('div');
    flex.className = "uk-flex";
    item.appendChild(flex);

    var text_desc = document.createElement('div');
    text_desc.style.width = "200px";
    text_desc.style.paddingRight = "10px";
    flex.appendChild(text_desc);

    var person ="?", caption ="?", finger ="?", shift ="?", gain ="?";

    if(!filename_regex.test(filename)){
        console.error("Wrong filename format:", filename);
        return;
    }
    
    var matchObj = filename_regex.exec(filename);

    if ( matchObj.groups ){
        if (matchObj.groups.person) { person = matchObj.groups.person; }
        if (matchObj.groups.caption) { caption = matchObj.groups.caption; }
        if (matchObj.groups.finger) { finger = matchObj.groups.finger; }
        if (matchObj.groups.shift) { shift = matchObj.groups.shift; }
        if (matchObj.groups.gain) { gain = matchObj.groups.gain; }
    }

    make_item_text_desc(text_desc, "Persona", person);
    make_item_text_desc(text_desc, "Caption", caption);
    make_item_text_desc(text_desc, "Finger", finger);
    make_item_text_desc(text_desc, "Shift", shift);
    make_item_text_desc(text_desc, "Gain", gain);
    
    //<img data-src="" width="192" height="192" alt="" uk-img>
    var img0 = document.createElement('img');
    img0.setAttribute("src", obj.output_any_snap );
    img0.setAttribute("width","192");
    img0.setAttribute("height","192");
    img0.setAttribute("alt","");
    flex.appendChild(img0);

    //<img data-src="" width="192" height="192" alt="" uk-img>
    var img1 = document.createElement('img');
    img1.setAttribute("src", obj.output_any_snap );
    img1.setAttribute("width","192");
    img1.setAttribute("height","192");
    img1.setAttribute("alt","");
    flex.appendChild(img1);

    //<hr class="uk-divider-icon">
    var hr = document.createElement('hr');
    hr.className = "uk-divider-icon";
    item.appendChild(hr);

    target_list.appendChild(item);
    console.log(obj);
}

function make_item(filename){
    var ref_c5 = child_process.spawnSync(
        './app/bin/ref_c5', 
        [ '-i', './app/data/' + filename] );
    
    if(ref_c5.error) {
        console.log("ERROR: ",ref_c5.error);
        console.log("stderr: ",ref_c5.stderr);
        console.log("exist code: ",ref_c5.status);
    }

    try {
        var obj = JSON.parse(ref_c5.stdout);
        make_item_html(obj, filename);
    } catch(e) {
        console.error(filename);
        console.error(e);        
    }    
}

fs.readdir(dataFolder, (err, files) => {
    bar.value = 0;

    var bar_value_part = 100 / (files.length + 1);

    files.forEach(file => {
        bar.value += bar_value_part;
        make_item(file);
    });

    bar.value = 100;
});